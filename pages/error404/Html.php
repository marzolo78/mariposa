<?php 
$this->topBar->show();
?>
<div id="error-container" class="center_vertical">
    <div id="error-title">Oops - Seite nicht gefunden.</div>
    <p>Leider wurde die gewünschte Seite nicht gefunden.</p>
    <a href="/"><div id="error-btn">ZURÜCK ZUR STARTZEITE</div></a>
</div>
