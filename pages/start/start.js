$(window).on('load', function() {
    $('#loading_container').css('display', 'none');
    $('#top_bar').fadeIn(500);
    $('#main_container').fadeIn(500);
    $('#bottom_bar_red_line').fadeIn(500);
    $('#bottom_bar').fadeIn(500);
    $('#bottom_bar_background').fadeIn(500);
});

/*$(document).ready(function() { 
    var cookies = new Cookies();
    var scrollPosition = cookies.getCookies("scrollPosition");

    if(scrollPosition !== "") {
        $(window).scrollTop(scrollPosition);
    }
});*/


