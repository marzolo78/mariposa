<!--Loading Message-->
<div id="loading_container" class="center_vertical">
    <p>Kinderhaus Mariposa e.V.</p>
    <p>Spanisch-Deutsche Elterninitiative</p>
    <img src="/static/images/shared/butterf_animation.gif" alt="butterf_animation"/>
    <p>Loading...</p>
</div>

<!--Top bar-->
<?php $this->topBar->show(); ?>

<div id="main_container" class="main_width main_padding_left">
    <!--Navigation-->
    <?php $this->navi->show(); ?>
    
    <!--Interviews-->
    <?php $this->interviews->show(); ?>
    
    <!--Team-->
    <?php $this->team->show(); ?>
    
    <!--House-->
    <?php $this->house->show(); ?>
    
	<!--Info-->
    <?php $this->info->show(); ?>
	
    <!--Contact-->
    <?php $this->contact->show(); ?>
	
</div>
<!--Bottom Bar-->
<?php $this->bottomBar->show(); 


