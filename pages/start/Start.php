<?php
include_once 'components/document_head/Head.php';
include_once 'components/top_bar/TopBar.php';
include_once 'components/navi/Navi.php';
include_once 'components/interviews/Interviews.php';
include_once 'components/team/Team.php';
include_once 'components/house/House.php';
include_once 'components/contact/Contact.php';
include_once 'components/info/Info.php';
include_once 'components/bottom_bar/BottomBar.php';
include_once 'classes/Services.php';
include_once 'interfaces/Page.php';
include_once 'classes/Language.php';
include_once 'classes/PageName.php';

class Start extends Services implements Page
{     
    public $headTitle;
    public $language;
    public $topBar;
    public $navi;
    public $interviews;
    public $team;
    public $house;
	public $info;
    public $contact;
    public $bottomBar;
    public $pageName;
    
    const JS_PATH = "/pages/start/start.js";
    
    public function renderPage()
    {
        $nameClass = get_class();
        $this->language = $this->getLanguage();
        
        $pageTitle = "Kindergarten Mariposa e.V. - Spanisch-deutsche Elterninitiative";
        $this->setDocumentHead($this->language, $this->getPageName($this->language, $pageTitle, $pageTitle), "pages/start/start.css");
        
        $this->topBar = new TopBar($this->language);
        $this->topBar->setPageName($this->pageName);
        $this->navi = new Navi($this->language);
        $this->interviews = new Interviews($this->language);
        $this->team = new Team($this->language);
        $this->house = new House($this->language);
		$this->info = new Info($this->language);
        $this->contact = new Contact($this->language);
        $this->bottomBar = new BottomBar($this->language);
                
        $this->renderBody($nameClass);
    }
    
    public function addJS($path) {
        return parent::addJS($path);
    }
    
    public function setLanguage($language = null)
    {
        parent::setLanguage($language);
    }

    public function setDocumentHead($language = null, $pageName = null, $cssPath = null)
    {
        parent::setDocumentHead($language, $pageName, $cssPath);
    }
    
    public function getDocumentText($className)
    {
        return $this->getTextObject($this->language, $className);
    }
    
    public function setPageName($pageName)
    {
        $this->pageName = $pageName;
    }
}
 