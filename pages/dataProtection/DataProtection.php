<?php
include_once 'components/top_bar/TopBar.php';
include_once 'interfaces/Page.php';
include_once 'components/document_head/Head.php';
include_once 'classes/Services.php';

class DataProtection extends Services implements Page
{
    public $topBar;
    
    const JS_PATH = "/pages/dataProtection/dataProtection.js";
    
    public function renderPage()
    {
        $nameClass = get_class();
        
        $this->setDocumentHead($this->getLanguage(), PageName::DATA_PROTECTION_TITLE, "/pages/dataProtection/dataProtection.css");
        
        $this->topBar = new TopBar($this->getLanguage(), $nameClass);
        
        $this->renderBody($nameClass);
    }
    
    public function addJS($path) {
        return parent::addJS($path);
    }
    
    public function setLanguage($language = null)
    {
        parent::setLanguage($language);
    }

    public function setDocumentHead($language = null, $pageName = null, $cssPath = null) {
        parent::setDocumentHead($language, $pageName, $cssPath);
    }
    
    public function getDocumentText($nameClass) {
         
    }

    public function setPageName($pageName) {
        
    }
}
