<?php 
$this->topBar->show();
?>
<div id="data-p-container">
    <a href="/" class="link question"><div>ZURÜCK ZUR STARTZEITE</div></a>
    <h1 id="data-p-title">Datenschutzerklärung</h1>
    <h3>I. Information über die Erhebung personenbezogener Daten</h3>
    <p>1. Im Folgenden informieren wir über die Erhebung personenbezogener Daten bei Nutzung unserer Website. Personenbezogene Daten sind alle Daten, die auf Sie persönlich beziehbar sind, z. B. Name, Adresse, E-Mail-Adressen, Nutzerverhalten.</p><br/>
    <p>2. Verantwortlicher gem. Art. 4 Abs. 7 EU-Datenschutz-Grundverordnung (DS-GVO) ist die Elterninitiative Kinderhaus Mariposa e.V., Heubergstrasse 31, 81825 München, E-Mail: <a href="mailto:contacto@kindergarten-mariposa.org"><span class="question link">CONTACTO@KINDERGARTEN-MARIPOSA.ORG</span></a></p><br/>
    <p>3. Bei Ihrer Kontaktaufnahme mit uns per E-Mail oder über ein Kontaktformular werden die von Ihnen mitgeteilten Daten (etwa Ihre E-Mail-Adresse, ggf. weitere Angaben wie bspw. Ihr Name und Ihre Telefonnummer) von uns gespeichert, um Ihre Anliegen zu beantworten. Die in diesem Zusammenhang anfallenden Daten löschen wir, nachdem die Speicherung nicht mehr erforderlich ist, oder schränken die Verarbeitung ein, falls gesetzliche Aufbewahrungspflichten bestehen.</p><br/>
    <h3>II. Ihre Rechte</h3>
    <p>1. Sie haben gegenüber uns folgende Rechte hinsichtlich der Sie betreffenden personenbezogenen Daten:</p>
    <ul>
        <li>Recht auf Auskunft,</li>
        <li>Recht auf Berichtigung oder Löschung,</li>
        <li>Recht auf Einschränkung der Verarbeitung,</li>
        <li>Recht auf Widerspruch gegen die Verarbeitung,</li>
        <li>Recht auf Datenübertragbarkeit.</li>
    </ul>

    <p>2. Sie haben zudem das Recht, sich bei einer Datenschutz-Aufsichtsbehörde über die Verarbeitung Ihrer personenbezogenen Daten durch uns zu beschweren.</p><br/>

    <h3>III. Erhebung personenbezogener Daten bei Besuch unserer Website</h3>
    <p>1. Bei der Nutzung unser Website speichern wir keine personenbezogenen Daten.</p><br/>
    <p>2. Es werde keine Cookies auf dieser Seite eigesetzt.</p><br/>

    <h3>IV. Widerspruch oder Widerruf gegen die Verarbeitung Ihrer Daten</h3>
    <p>1.Falls Sie eine Einwilligung zur Verarbeitung Ihrer Daten erteilt haben, können Sie diese jederzeit widerrufen.</p><br/>
    <p>2. Soweit wir die Verarbeitung Ihrer personenbezogenen Daten auf die Interessenabwägung stützen, können Sie Widerspruch gegen die Verarbeitung einlegen. Dies ist der Fall, wenn die Verarbeitung insbesondere nicht zur Erfüllung eines Vertrags mit Ihnen erforderlich ist, was von uns jeweils bei der nachfolgenden Beschreibung der Funktionen dargestellt wird. Bei Ausübung eines solchen Widerspruchs bitten wir um Darlegung der Gründe, weshalb wir Ihre personenbezogenen Daten nicht wie von uns durchgeführt verarbeiten sollten. Im Falle Ihres begründeten Widerspruchs prüfen wir die Sachlage und werden entweder die Datenverarbeitung einstellen bzw. anpassen oder Ihnen unsere zwingenden schutzwürdigen Gründe aufzeigen, aufgrund derer wir die Verarbeitung fortführen.</p><br/>

    <h3>V. Änderung dieser Datenschutzerklärung</h3>
    <p>Durch die Weiterentwicklung unserer Website und Angebote darüber oder aufgrund geänderter gesetzlicher beziehungsweise behördlicher Vorgaben kann es notwendig werden, diese Datenschutzerklärung zu ändern. Die jeweils aktuelle Datenschutzerklärung kann jederzeit auf der Website von Ihnen abgerufen und ausgedruckt werden.</p><br/>
    <a href="/" class="link question"><div>Zurück zur Startseite</div></a>
</div>
