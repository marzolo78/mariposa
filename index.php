<?php
//error_reporting(E_ALL);
error_reporting(0);

include_once 'classes/PageName.php';
include_once 'classes/Language.php';

define("DOMAIN_NAME",$_SERVER['HTTP_HOST']);
$language = null;
const MIN_PATH_ITEMS = 2;
$pagesPath = 'pages/';
$pathItem = null;
$changeLanguage = null;

$url = strtok($_SERVER["REQUEST_URI"],'?');
$pathItems = explode('/', substr($url, 1));

if(isset($pathItems[0]) && empty($pathItems[0]) && count($pathItems) < MIN_PATH_ITEMS)
{
   $pathItem = PageName::MAIN_FILE;
   $language = Language::GERMAN;
   $changeLanguage = PageName::MAIN_SPANISH;
}
elseif(isset($pathItems[0]) && count($pathItems) < MIN_PATH_ITEMS)
{
    switch($pathItems[0])
    {
        case PageName::MAIN_GERMAN:
            $pathItem = PageName::MAIN_FILE;
            $language = Language::GERMAN;
            $changeLanguage = PageName::MAIN_SPANISH;
            break;
        case PageName::MAIN_SPANISH:
            $pathItem = PageName::MAIN_FILE;
            $language = Language::SPANISH;
            $changeLanguage = "";
            break;
        case PageName::DATA_PROTECTION:
            $pathItem = PageName::DATA_PROTECTION_FILE;
            $language = Language::GERMAN;
            $changeLanguage = "";
            break;
        case PageName::OLD_MAIN_SPANISH:
            redirect(PageName::MAIN_SPANISH);
            break;
        case PageName::OLD_MAIN_GERMAN:
            redirect(PageName::MAIN_GERMAN);
            break;
        case "index.php":
            redirect(PageName::MAIN_GERMAN);
            break;
        default:
            $pathItem = PageName::ERROR; 
    }
}
elseif(isset($pathItems[0]) && isset($pathItems[1]) && count($pathItems) < 3)
{
    switch($pathItems[0]) {
       case PageName::OLD_GERMAN:
            if($pathItems[1] == PageName::OLD_CONTACT_GERMAN)
            {
                redirect(PageName::MAIN_GERMAN);
            }
            elseif($pathItems[1] == PageName::OLD_CONCEPT_GERMAN)
            {
                redirect(PageName::MAIN_GERMAN);
            }
            elseif($pathItems[1] == PageName::OLD_TEACHERS_GERMAN)
            {
                redirect(PageName::MAIN_GERMAN);
            }
            elseif($pathItems[1] == PageName::OLD_HOUSE_GERMAN)
            {
                redirect(PageName::MAIN_GERMAN);
            }
            elseif($pathItems[1] == PageName::OLD_TIMES_GERMAN)
            {
                redirect(PageName::MAIN_GERMAN);
            }
            elseif($pathItems[1] == PageName::OLD_FUTURES_GERMAN)
            {
                redirect(PageName::MAIN_GERMAN);
            }
            elseif($pathItems[1] == PageName::OLD_LINKS_GERMAN)
            {
                redirect(PageName::MAIN_GERMAN);
            }
            else {
               $pathItem = PageName::ERROR;  
            }
           break;
       case PageName::OLD_SPANISH:
            if($pathItems[1] == PageName::OLD_CONTACT_SPANISH)
            {
                redirect(PageName::MAIN_SPANISH);
            }
            elseif($pathItems[1] == PageName::OLD_CONCEPT_SPANISH)
            {
                redirect(PageName::MAIN_SPANISH);
            }
            elseif($pathItems[1] == PageName::OLD_TEACHERS_SPANISH)
            {
                redirect(PageName::MAIN_SPANISH);
            }
            elseif($pathItems[1] == PageName::OLD_HOUSE_SPANISH)
            {
                redirect(PageName::MAIN_SPANISH);
            }
            elseif($pathItems[1] == PageName::OLD_TIMES_SPANISH)
            {
                redirect(PageName::MAIN_SPANISH);
            }
            elseif($pathItems[1] == PageName::OLD_FUTURES_SPANISH)
            {
                redirect(PageName::MAIN_SPANISH);
            }
            elseif($pathItems[1] == PageName::OLD_LINKS_SPANISH)
            {
                redirect(PageName::MAIN_SPANISH);
            }
            else {
              $pathItem = PageName::ERROR;   
            }
           break;
       default:
           $pathItem = PageName::ERROR;
           break;
    }
}
else
{
    $pathItem = PageName::ERROR;
}

include_once $pagesPath . $pathItem . '/' . ucwords($pathItem) . '.php';

$page = new $pathItem();
if($language != null)
{
    $page->setLanguage($language);
}
if($changeLanguage != null)
    {
    $page->setPageName($changeLanguage);
}

$page->renderPage();

echo '<script src="/js/jquery/jquery-3.1.1.min.js"></script>';
echo '<script src="/js/main.js"></script>';
echo $page->addJS($page::JS_PATH);

if(isset($page->topBar))
{
   echo $page->topBar->includeJS();
}
if(isset($page->navi))
{
   echo $page->navi->includeJS();
}
if(isset($page->interviews))
{
   echo $page->interviews->includeJS();
}
if(isset($page->team))
{
   echo $page->team->includeJS();
}

if(isset($page->house))
{
   echo $page->house->includeJS();
}

echo '</body>';
echo '</html>';

function redirect($pageName) {
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: http://$_SERVER[HTTP_HOST]/" . $pageName);
    exit();
}

