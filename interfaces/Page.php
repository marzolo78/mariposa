<?php

interface Page
{
    public function getDocumentText($className);
    public function setDocumentHead($language, $pageName, $cssPath);
    public function setPageName($pageName);
    public function addJS($path);
}
