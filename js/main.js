function getLanguage() {
    var language = $('#language').attr('alt');
    if(language === "german") {
        return  1;
    }else {
        return  2;
    }
}

function topScroll($viewEvent, $toView, topBarHeight) {
        $viewEvent.on('click', function(){
            try {
            $('html,body').animate({scrollTop: $toView.offset().top - topBarHeight});
            }catch(e) {
               
            }
        }); 
    };

function ScrollTo() {
    this.mainContainer = null;
    this.forNaviComponent = false;
    
    this.setMainContainer = function(view) {
        this.mainContainer = view;
    };
    
    this.setForNaviComponent = function(enable) {
        this.forNaviComponent = enable;
    };
    
    this.go = function() {
        var topBarHeight = $('#top_bar').height();
        var $document = $('html,body');
        var e = this.forNaviComponent;
    
        jQuery(this.mainContainer).on('click', function() {
            switch($(this).index()+1) {
                case 1:
                    $document.animate({scrollTop: $("#interview_img_container_1").offset().top - topBarHeight});
                    break;
                case 2:
                    $document.animate({scrollTop: $("#interview_img_container_2").offset().top - topBarHeight});
                    break;
                case 3:
                    if(!e) {
                        $document.animate({scrollTop: $("#team_container").offset().top - topBarHeight});
                    }
                    break;
                case 4:
                    if(!e) {
                        $document.animate({scrollTop: $("#house_container").offset().top - topBarHeight});
                    }
                    break;
                case 5:
                    $document.animate({scrollTop: $("#team_container").offset().top - topBarHeight});
                    break;
                case 6:
                    $document.animate({scrollTop: $("#house_container").offset().top - topBarHeight});
                    break;
            }
        });
    };
}

function ChangeButtonColorGroup() {
    this.mainContainer = null;
    this.imgPath = null;
    this.fileNames = null;
    this.fileFormat = null;
    this.firstColor = null;
    this.secondColor = null;
    
    this.setMainContainer = function(view) {
        this.mainContainer = view;
    };
    
    this.setImgPath = function(imgPath) {
        this.imgPath = imgPath;
    };
    
    this.setFileNames = function(fileNames) {
        this.fileNames = fileNames;
    };
    
    this.setFileFormat = function(fileFormat) {
        this.fileFormat = fileFormat;
    };
    
    this.setColorOrder = function(firstColor, secondColor) {
        this.firstColor = firstColor;
        this.secondColor = secondColor;
    };
    
    this.go = function() {
        var path = null;
        var PATH = this.imgPath;
        var mainContainer = this.mainContainer;
        var fileNames = this.fileNames;
        var fileFormat = this.fileFormat;
        var firstColor = this.firstColor;
        var secondColor = this.secondColor;
        
        jQuery(mainContainer).hover(function() {
        switch(jQuery(mainContainer).index(this)) {
            case 0:
                path =  PATH + fileNames[0] + '_' + firstColor + '.' + fileFormat;
                break;
            case 1:
                path =  PATH + fileNames[1] + '_' + firstColor + '.' + fileFormat;
                break;
            case 2:
                path =  PATH + fileNames[2] + '_' + firstColor + '.' + fileFormat;
                break;
            case 3:
                path =  PATH + fileNames[3] + '_' + firstColor + '.' + fileFormat;
                break;
        }
        
        $(this).attr('src', path);

    }, function() {
        var path = null;
        switch( jQuery(mainContainer).index(this)) {
            case 0:
                path =  PATH + fileNames[0] + '_' + secondColor + '.' + fileFormat;
                break;
            case 1:
                path =  PATH + fileNames[1] + '_' + secondColor + '.' + fileFormat;
                break;
            case 2:
                path =  PATH + fileNames[2] + '_' + secondColor + '.' + fileFormat;
                break;
            case 3:
                path =  PATH + fileNames[3] + '_' + secondColor + '.' + fileFormat;
                break;
        }
        
        $(this).attr('src', path);
    });
    };
}

function ChangeButtonColorSingle() {
    this.mainContainer = null;
    this.frontFile = null;
    this.backFile = null;
    var IMG_PATH = "/static/images/buttons/";
    
    this.setMainContainer = function(view) {
        this.mainContainer = view;
    };
    
    this.setFrontFile = function(file) {
        this.frontFile = file;
    };
    
    this.setBackFile = function(file) {
        this.backFile = file;
    };
    
    this.go = function() {
        var mainContainer = this.mainContainer;
        var f = this.frontFile;
        var b = this.backFile;
        
        jQuery(mainContainer).hover(function() {
             $(this).attr('src', IMG_PATH + b);
        }, function() {
             $(this).attr('src', IMG_PATH + f);
        });
    };
}

function FadeIOGroup() {
    this.mainContainer = null;
    this.backImg = null;
    this.frontImg = null;
    this.timing = 1000;
    this.disables = null;
    
     this.setMainContainer = function(view) {
        this.mainContainer = view;
    };
    
    this.setBackImg = function(stringID) {
        this.backImg = stringID;
    };
    
    this.setFrontImg = function(stringID) {
        this.frontImg = stringID;
    };
    
    this.setTiming = function(time) {
        this.timing = time;
    };
    
    this.setDisables = function(disables) {
        this.disables = disables;
    };
    
    this.go = function() {
        var c = this.mainContainer;
        var f = this.frontImg;
        var b = this.backImg;
        var t = this.timing;
        var indexImage = null;
        var d = this.disables;
        
        var disableAnimationFor = function(indexImage) {
            for(var i = 0; i < d.length; i++) {
                if(indexImage === d[i]) {
                   return true; 
                }
            }
            return false;
        };
        
          jQuery(c).hover(function() {
            var indexContainer =  jQuery(c).index(this);
            var offSet = (indexContainer + 1);

            indexImage = offSet;
        
            if(disableAnimationFor(indexImage)) {
                return;
            }
            
            $(b + indexImage).css('display', 'block'); 
            $(f + indexImage).stop(true).fadeTo(t, 0); 

        }, function() {
             if(disableAnimationFor(indexImage)) {
                return;
            }

            $(f + indexImage).stop(true).fadeTo(t, 1);
        });
    };
}

function IsInView(vZero, vRoot) {
    this.vZero = vZero;
    this.vRoot = vRoot;
    this.getView = function() {
        return this.vRoot;
    };
    
    this.go = function() {
        var elemTop = this.vZero.getBoundingClientRect().top;
        var elemBottom = this.vZero.getBoundingClientRect().bottom;

        var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
        return isVisible;
    };
}

function Cookies() {
    
    this.setCookies = function(cname,cvalue,time) {
        var d = new Date();
        d.setTime(d.getTime() + (time * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    };
    
    this.getCookies = function(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };
}

function AlternateImgBoxMargins() {
    this.offset = null;
    this.MULTIPLE = null;
    this.sumMultiple = null;
    this.view = null;
    this.alternate = false;
    this.className = null;
    
    this.setMultiples = function(multiple) {
        this.MULTIPLE = multiple;
        this.sumMultiple = multiple;
    };
    
    this.setView = function($view) {
        this.view = $view;
    };
    
    this.setClassName = function(name) {
        this.className = name;
    };
    
    this.go = function() {
        if(this.MULTIPLE === null || this.sumMultiple === null || this.view === null || this.className === null) {
            console.log("Set every data!");
            return;
        }
        
        for(var i = 0; i < this.view.length; i++) {
            this.offset = i+1;
            if(this.offset <= this.sumMultiple  && !this.alternate) {
                if(this.offset % 2 !== 0) {
                    $(this.className + this.offset).css('margin-top', '70px');
                }
                if(this.offset % 3 === 0) {
                    this.sumMultiple = this.sumMultiple + 3;
                    this.alternate = true;
                }
            }else if(this.offset <= this.sumMultiple) {
                if(this.offset % 2 === 0 ) {
                    $(this.className + this.offset).css('margin-top', '70px');
                }

                if(this.offset % 3 === 0) {
                    this.sumMultiple = this.sumMultiple + 3;
                    this.alternate = false;
                }
            }
        } 
    };
}
    