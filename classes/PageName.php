<?php
class PageName
{
    const MAIN_FILE = "start";
    const ERROR_FILE = "error404";
    const DATA_PROTECTION_FILE = "dataProtection";
    
    const MAIN_GERMAN = "";
    const MAIN_SPANISH = "inicio";
    
    const ERROR = "error404";
    const ERROR_PAGE_TITLE = "Seite nicht gefunden | Kindergarten Mariposa";
    
    const DATA_PROTECTION = "datenschutz";
    const DATA_PROTECTION_TITLE = "Datenschutz | Kindergarten Mariposa";
    
    const BASE_FILE = "index.php";
    
    const OLD_MAIN_GERMAN = "index.html";
    const OLD_MAIN_SPANISH = "index_es.html";
    const OLD_CONTACT_GERMAN = "kontakt.html";
    const OLD_CONTACT_SPANISH = "contacto.html";
    const OLD_CONCEPT_GERMAN = "konzept.html";
    const OLD_CONCEPT_SPANISH = "concepto.html";
    const OLD_TEACHERS_GERMAN = "erzieherinnen.html";
    const OLD_TEACHERS_SPANISH = "equipo.html";
    const OLD_HOUSE_GERMAN = "haus.html";
    const OLD_HOUSE_SPANISH = "edificio.html";
    const OLD_TIMES_GERMAN = "betreuungszeiten.html";
    const OLD_TIMES_SPANISH = "horarios.html";
    const OLD_FUTURES_GERMAN = "zukunft.html";
    const OLD_FUTURES_SPANISH = "futuro.html";
    const OLD_LINKS_GERMAN = "links.html";
    const OLD_LINKS_SPANISH = "enlaces.html";
    
    const OLD_SPANISH = "es";
    const OLD_GERMAN = "de";
    
    const DEV_HOST_NAME = "mariposa.esy.es";
}

