<?php
include_once 'classes/Language.php';

class Services
{ 
    private $pagesPath = "pages/";
    private $language = Language::GERMAN;
    const DEFAULT_HEAD_TITLE = "Mariposa";


    protected function renderBody($nameClass)
    {
        require_once $this->pagesPath . lcfirst($nameClass) . "/Html.php";
    }
    
    protected function setLanguage($language)
    {
        $this->language = $language;
    }
    
    protected function getLanguage()
    {
        return $this->language;
    }
    
    protected function getTextObject($language, $nameClass)
    {
        $object = null;
        
        switch($language)
        {
            case Language::GERMAN:
                $object = $this->getText($nameClass)->german;
                break;
            case Language::SPANISH:
                $object = $this->getText($nameClass)->spanish;
                break;
        }
        
        return $object;
    }
    
    protected function setDocumentHead($language, $pageName, $cssPath = null)
    {
        $head = new Head();
        if($language == null)
        {
           $language =  Language::GERMAN;
        }
                
        if($pageName === PageName::ERROR_PAGE_TITLE)
        {
            $head->setRobot(false);
        }
        
        if($language === Language::GERMAN) {
            $head->setLang('de');
        }
        elseif($language === Language::SPANISH)
        { 
            $head->setLang('es');
        }
        
        $head->setHeadTitle($pageName != null ? $pageName : self::DEFAULT_HEAD_TITLE);
        
        if($cssPath != null)
        {
           $head->addCss($cssPath); 
        }
        $head->show();
    }
    
    protected function getPageName($language, $pageNameGerman, $pageNameSpanish)
    {
        $pageName = null;
        if($language == Language::GERMAN)
        {
           $pageName = $pageNameGerman;
        }
        elseif($language == Language::SPANISH)
        {
            $pageName = $pageNameSpanish;
        }
        
        return $pageName;
    }
    
    protected function addJS($path) {
        if($path != null)
        {
            return '<script src="' . $path . '"></script>';
        }
    }
  
    private function getText($className)
    {
        $text = file_get_contents($this->pagesPath . strtolower($className) . "/text.json");
        return json_decode($text);
    }
}




    
        
        
        
    
    


