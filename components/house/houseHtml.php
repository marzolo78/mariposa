<div id="house_container" class="components_margin_top">
    <?php foreach($this->content->houseBoxes as $key => $houseBoxe):?>
    <div class="team_containers team_boxes">
        <div id="house_container_<?php echo $key + 1 ?>" class="house_imgs_container">
            <?php if($key == 1):  ?>
            <img class="center_horizontal tile_buttons_width" src="<?php echo self::BUTTONS_IMGS_PATH . $houseBoxe->img ?>" alt="<?php echo $houseBoxe->img ?>"/>
            <?php elseif($key == 0):  ?>
            <img id="house_butterf_green" class="absolute" src="<?php echo self::SHARED_IMGS_PATH ?>/butterf_green.png" alt="house_butterf_green"/>
            <img class="center_horizontal" src="<?php echo self::SHARED_IMGS_PATH . $houseBoxe->img ?>" alt="<?php echo $houseBoxe->img ?>"/>
            <?php else: ?>
            <img class="center_horizontal" src="<?php echo self::IMGS_PATH . $houseBoxe->img ?>" alt="<?php echo $houseBoxe->img ?>"/>
            <?php endif; ?>
        </div>
         <?php if(isset($houseBoxe->title)): ?>
        <a href="https://www.google.com/maps/place/Heubergstraße+31,+81825+München,+Germany" target="_blank">
            <p  class="question link"><?php echo $houseBoxe->title->address ?></p>
        </a>
        <p class="intro"><?php echo $houseBoxe->title->text ?></p>
        <p id="house_description" class="answer text_size_medium"><?php echo $houseBoxe->description ?></p>
        <?php endif; ?>
    </div>
    <?php endforeach; ?>
</div>

