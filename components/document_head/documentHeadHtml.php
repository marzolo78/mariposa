<!DOCTYPE html>
<html lang="<?php echo $this->lang ?>">
     <head>
        <meta charset="UTF-8">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <title><?php echo $this->title ?></title>
        <link rel="shortcut icon" href="/static/images/schmetterling_favicon.ico">
        <?php if($this->robot && constant("DOMAIN_NAME") != PageName::DEV_HOST_NAME): ?>
        <meta name="robots" content="index, follow">
        <?php else: ?>
        <meta name="robots" content="noindex, nofollow">
        <?php endif;; ?>
        <meta name="description" content="Kindergarten Mariposa e.V. - Spanisch-deutsche Elterninitiative in München">
        <meta name="keywords" content="Kindergarten, guardería, jardin de infancia, München, Múnich, Mariposa, Kinderbetreuung, zweisprachig, bilingüe, educación preescolar, spanisch, deutsch, español, aleman">
        <meta name="author" content="Kindergarten Mariposa e.V.">
        <meta name="audience" content="Alle">
        <?php if($this->fileName != null): ?>
        <link rel="stylesheet" href="<?php echo $this->fileName ?>"/>
        <?php endif; ?>
    </head>
    <body>

