<?php
include_once 'classes/PageName.php';

class Head
{
    public $title = null;
    public $robot = true;
    public $lang = null;
    public $fileName = null;
    const HTML = "/Html.php";

    public function show()
    {
        include_once 'components/document_head/documentHeadHtml.php'; 
    }
    
    public function setHeadTitle($title)
    {
        $this->title = $title;
    }
    
    public function setRobot($index = true)
    {   
        if(!$index)
        {
           $this->robot = false; 
        }
    }
    
    public function setLang($lang)
    {
        $this->lang = $lang; 
    }
    
    public function addCss($fileName)
    {
        $this->fileName = $fileName;
    }
}




    
        
        
        
    
    


