<div id="bottom_bar_red_line" class="components_margin_top"></div>
<div id="bottom_bar" class="main_width main_padding_left">
    <div>
        <p>Copyright 2017,</p>
        <p>Kinderhaus Mariposa e.V.</p>
    </div>
    <div>
        <p>Die Elterninitiative Mariposa e.V.</p>
        <p>wird unterstützt von der <br/>Landeshauptstadt München</p>
        <p>Referat für Bildung und Sport</p>
    </div>
    <div>
        <img src="<?php echo self::IMGS_PATH .  "jugendamt-logo.jpg"?>" alt="Landeshauptstadt Muenchen"/>
    </div>
</div>
<div id="bottom_bar_background" class="bar top_bar_top_content_height"></div>

