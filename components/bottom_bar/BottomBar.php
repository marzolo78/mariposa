<?php

class BottomBar
{
    public $language;
    public $content;
    const IMGS_PATH = "/static/images/bottom_bar/";
    const BUTTONS_IMGS_PATH = "/static/images/buttons/";
    const BOTTOM_BAR_JS = "/components/bottom_bar/bottomBar.js";


    public function __construct($language) {
        $this->language = $language;
    }
    
    public function show()
    {
        $this->content = $this->getContent($this->language);
        include_once 'components/bottom_bar/bottomBarHtml.php';
    }
    
    public function includeJS()
    {
        return '<script src="' . self::BOTTOM_BAR_JS . '"></script>';
    }
    
    private function getContent($language) {
        $object = null;
        $json = json_decode(file_get_contents('components/bottom_bar/content.json'));

        switch($language) {
            case Language::GERMAN:
                $object = $json->german;
                break;
            case Language::SPANISH:
                $object = $json->spanish;
                break;
        }
        
        return $object;
    }
}

