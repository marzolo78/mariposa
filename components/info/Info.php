<?php

class Info
{
    public $language;
    public $content;
    const IMGS_PATH = "/static/images/info/";
    const BUTTONS_IMGS_PATH = "/static/images/buttons/";
    const JS_PATH = "/components/info/info.js";


    public function __construct($language) {
        $this->language = $language;
    }
    
    public function show()
    {
        $this->content = $this->getContent($this->language);
        include_once 'components/info/infoHtml.php';
    }
    
    public function includeJS()
    {
        return '<script src="' . self::JS_PATH . '"></script>';
    }
    
    private function getContent($language) {
        $object = null;
        $json = json_decode(file_get_contents('components/info/content.json'));

        switch($language) {
            case Language::GERMAN:
                $object = $json->german;
                break;
            case Language::SPANISH:
                $object = $json->spanish;
                break;
        }
        
        return $object;
    }
}

