<div id="info_container" class="components_margin_top">
    <div id="info_info1">
        <img src="<?php echo self::BUTTONS_IMGS_PATH . $this->content->buttonFile ?>" alt="mariposa_info_title"/>
    </div>
	<div id="info_info2">
        <p class="intro info_margin_top"><?php echo $this->content->title1 ?></p>
        <p class="info text_size_medium info_margin_topsmall"><?php echo $this->content->text1 ?></p>
    </div>
	<div id="info_info3">
        <p class="intro info_margin_top"><?php echo $this->content->title2 ?></p>
        <p class="info text_size_medium info_margin_topsmall"><?php echo $this->content->text2 ?></p>
    </div>
</div>

