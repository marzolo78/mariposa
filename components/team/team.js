$(document).ready(function() {
    var fadeIOGroup = new FadeIOGroup();
    fadeIOGroup.setMainContainer($('.team_containers'));
    fadeIOGroup.setBackImg('#team_back_img_');
    fadeIOGroup.setFrontImg('#team_img_');
    fadeIOGroup.setDisables([]);
    fadeIOGroup.setTiming(1000);
    fadeIOGroup.go();
    
    var teamImgBoxMargins = new AlternateImgBoxMargins();
    teamImgBoxMargins.setView($('.team_containers'));
    teamImgBoxMargins.setMultiples(3);
    teamImgBoxMargins.setClassName('#team_container_');
    teamImgBoxMargins.go();
    
    $('.team_quote').hover(function() {
        $(this).css('opacity', '1');
    }, function() {
        $(this).css('opacity', '0');
    });
    
    
});



