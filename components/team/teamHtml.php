<?php  if($this->language === Language::GERMAN): ?>
<div id="team_container">
<?php  elseif($this->language === Language::SPANISH): ?>
    <div id="team_container">
<?php endif; ?>
    <img id="team_title" class="tile_buttons_width" src="<?php echo self::BUTTONS_IMGS_PATH .$this->content->teamTitle ?>" alt="mariposa_team_title"/>
    
    <?php foreach($this->content->teamBoxes as $key => $item): ?>
    <div id="<?php echo "team_container_" . ($key+1) ?>" class="team_containers team_boxes">
        <?php if(isset($item->imgBack)): ?>
            <div class="team_imgs_container">
                <img id="<?php echo "team_back_img_" . ($key+1) ?>" class="navi_background_img center_horizontal" src="<?php echo ($key !== 1 ? self::IMGS_PATH : self::SHARED_IMGS_PATH) . $item->imgBack ?>" alt="<?php echo $item->imgBack ?>"/>
                <img id="<?php echo "team_img_" . ($key+1) ?>" class="center_horizontal" src="<?php echo ($key !== 1 ? self::IMGS_PATH : self::SHARED_IMGS_PATH) . $item->imgFront ?>" alt="<?php echo $item->imgFront ?>"/>
            </div>
        <div class="team_info_container">
            <div class="team_quote interview_quotes"><p><?php echo $item->quote ?></p></div>
            <?php if(isset($item->info)): ?>
                <?php foreach($item->info as $item): ?>
                    <?php echo $item ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <?php endif; ?>
    </div>
    <?php endforeach;?>
</div>

