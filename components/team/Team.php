<?php

class Team
{
    public $language;
    public $content;
    const IMGS_PATH = "/static/images/team/";
    const SHARED_IMGS_PATH = "/static/images/shared/";
    const BUTTONS_IMGS_PATH = "/static/images/buttons/";
    const JS_PATH = "/components/team/team.js";


    public function __construct($language) {
        $this->language = $language;
    }
    
    public function show()
    {
        $this->content = $this->getContent($this->language);
        include_once 'components/team/teamHtml.php';
    }
    
    public function includeJS()
    {
        return '<script src="' . self::JS_PATH . '"></script>';
    }
    
    private function getContent($language) {
        $object = null;
        $json = json_decode(file_get_contents('components/team/content.json'));

        switch($language) {
            case Language::GERMAN:
                $object = $json->german;
                break;
            case Language::SPANISH:
                $object = $json->spanish;
                break;
        }
        
        return $object;
    }
}

