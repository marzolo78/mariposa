$(document).ready(function() {
    var fileNames = null;
    var language = getLanguage();
    if(language === 1) {
        fileNames = ["wer_sind_wir", "was_bitten_wir", "das_team", "das_haus"];
    }else if(language === 2) {
        fileNames = ["quienes_somos", "que_ofrecemos", "el_equipo", "la_casa"];
    }
    
    var changeButtonColorGroup = new ChangeButtonColorGroup();
    changeButtonColorGroup.setMainContainer($('.navi_containers img:nth-child(3)'));
    changeButtonColorGroup.setImgPath('/static/images/buttons/');
    changeButtonColorGroup.setFileNames(fileNames);
    changeButtonColorGroup.setFileFormat('svg');
    changeButtonColorGroup.setColorOrder('red', 'green');
    changeButtonColorGroup.go();

    var fadeIOGroup = new FadeIOGroup();
    fadeIOGroup.setMainContainer($('.navi_containers'));
    fadeIOGroup.setBackImg('#navi_background_img_');
    fadeIOGroup.setFrontImg('#navi_img_');
    fadeIOGroup.setDisables([3, 4]);
    fadeIOGroup.go();
    
    var naviScrollTo = new ScrollTo();
    naviScrollTo.setMainContainer($('#navis_container div'));
    naviScrollTo.setForNaviComponent(true);
    naviScrollTo.go();
});




