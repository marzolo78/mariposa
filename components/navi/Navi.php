<?php
include_once 'classes/Language.php';

class Navi
{
    private $language;
    public $content;
    public $showNotice = true;
    const IMGS_PATH = "/static/images/navi/";
    const SHARED_IMGS_PATH = "/static/images/shared/";
    const DOCUMENTS_PATH = "/static/documents/";
    const BUTTONS_IMGS_PATH = "/static/images/buttons/";
    const TOP_BAR_JS = "/components/navi/navi.js";
    
    public function __construct($language) {
        $this->language = $language;
    }
    
    public function show()
    {
        $this->content = $this->getContent();
        include_once 'components/navi/naviHtml.php';
    }
    
    public function includeJS()
    {
        return '<script src="' . self::TOP_BAR_JS . '"></script>';
    }
    
    private function getContent() {
        $json = json_decode(file_get_contents('components/navi/content.json'));
        $languageObject = null;
        
        switch($this->language)
        {
            case Language::GERMAN:
                $languageObject = $json->german;
                break;
             case Language::SPANISH:
                 $languageObject = $json->spanish;
                break;
        }
        
        return $languageObject;
    }
}
