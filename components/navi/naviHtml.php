<img id="navi_butterf_green" class="absolute" src="<?php echo self::SHARED_IMGS_PATH ?>/butterf_green.png" alt="navi_butterf_green"/>
<img id="navi_butterf_yellow" class="absolute" src="<?php echo self::SHARED_IMGS_PATH ?>/butterf_yellow.png" alt="navi_butterf_yellow"/>
<div id="navis_container">
    <?php foreach($this->content->imagesPath as $key => $item): ?>
    <?php $keyOffset = $key+1; ?>
    <div id="<?php echo "navi_img_container_" . $keyOffset ?>" class="navi_containers team_boxes">
        <?php if(isset($item->backgroundImg)): ?>
        <?PHP if($keyOffset != 3):?>
            <img id="<?php echo "navi_background_img_" . $keyOffset ?>" class="navi_background_img center_horizontal afterLoaded" src="<?php echo ($keyOffset !== 2 ? self::IMGS_PATH : self::SHARED_IMGS_PATH) . $item->backgroundImg ?>" alt="<?php echo $item->backgroundImg ?>"/>
        <?php else: ?>
        <img id="<?php echo "navi_background_img_" . $keyOffset ?>" class="center_horizontal" src="<?php echo self::IMGS_PATH . $item->backgroundImg ?>" alt="<?php echo $item->backgroundImg ?>"/>
        <?php endif; ?>
        <?php endif; ?>

        <?PHP if($keyOffset != 3):?>
            <img id="<?php echo "navi_img_" . $keyOffset ?>" class="center_horizontal" src="<?php echo self::IMGS_PATH . $item->frontImg ?>" alt="<?php echo $item->frontImg ?>"/>
        <?php else: ?>
            <?php if($this->showNotice): ?>
            <a href="<?php echo self::DOCUMENTS_PATH . $item->DocumentName ?>" target="_blank">
                <img id="notice" class="center_horizontal" src="<?php echo self::IMGS_PATH . $item->frontImg ?>" alt="wir_stellen_ein"/>
<!--                <span style="position: absolute; width: 190px; height: 152px; left: 90px; top: 145px; text-align: center; font-size: 2.1em; color: white;">
                    <?php if($this->language == 'german'): ?>
                    <span>Tag der offenen Tür!</span><br/>
                    <span style="font-size: 0.5em;">15.11.19, 15-18:00 Uhr</span><br/>
                    <span style='font-size: 0.7em; text-decoration: underline'>mehr Info</span>
                    <?php elseif($this->language == 'spanish'): ?>
                    <span>Día de puertas abiertas!</span><br/>
                    <span style="font-size: 0.5em;">15.11.19, 15-18:00 Uhr</span><br/>
                    <span style='font-size: 0.6em; text-decoration: underline'>más información</span>
                    <?php endif; ?>
                </span>-->
            </a>
            <?php endif; ?>
        <?php endif; ?>
        <?php if(isset($item->buttonImg)): ?>
            <img class="navi_buttons center_horizontal" src="<?php echo self::BUTTONS_IMGS_PATH . $item->buttonImg ?>" alt="<?php echo $item->buttonImg ?>"/>
        <?php endif; ?>
    </div>
    <?php endforeach;?>
</div> 

