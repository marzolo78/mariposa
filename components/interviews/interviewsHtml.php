<div id="interviews_container" class="components_margin_top">
    <img id="interviews_butterf_green" class="absolute" src="<?php echo self::SHARED_IMAGE_PATH ?>/butterf_green.png" alt="interviews_butterf_green"/>
    <img id="interviews_butterf_yellow" class="absolute" src="<?php echo self::SHARED_IMAGE_PATH ?>/butterf_yellow.png" alt="interviews_butterf_yellow"/>
    <?php foreach($this->content as $key => $item): ?>
        <?php echo $this->alternateView($key+1, $item) ?>
    <?php endforeach; ?>
</div>


