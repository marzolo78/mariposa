$(document).ready(function() {
    var imgFade = new FadeIOGroup();
    imgFade.setMainContainer($('.interview_img_container'));
    imgFade.setFrontImg('#interview_front_img_');
    imgFade.setBackImg('#interview_back_img_');
    imgFade.setDisables([]);
    imgFade.setTiming(1000);
    imgFade.go();
    
    var first = new ChangeButtonColorSingle();
    var frontImg = null;
    var backImg = null;
    var language = getLanguage();
    
    if(language === 1) {
        frontImg = 'wer_sind_wir_red.svg';
        backImg = 'wer_sind_wir_green.svg';
    }else {
        frontImg = 'quienes_somos_red.svg';
        backImg = 'quienes_somos_green.svg';
    }
    
    first.setMainContainer($('#interview_img_container_1 img:nth-child(3)'));
    first.setBackFile(frontImg);
    first.setFrontFile(backImg);
    first.go();
    
    if(language === 1) {
        frontImg = 'was_bitten_wir_red.svg';
        backImg = 'was_bitten_wir_green.svg';
    }else {
        frontImg = 'que_ofrecemos_red.svg';
        backImg = 'que_ofrecemos_green.svg';
    }
    
    var second = new ChangeButtonColorSingle();
    second.setMainContainer($('#interview_img_container_2 img:nth-child(3)'));
    second.setBackFile(frontImg);
    second.setFrontFile(backImg);
    second.go();
    
    var fadeTiming = 400;
    var top = 200;
    var interview_quote_1 = new IsInView($('#interview_quote_1')[0], $('#interview_quote_1'));
    var interview_quote_2 = new IsInView($('#interview_quote_2')[0], $('#interview_quote_2'));
    var interview_quote_3 = new IsInView($('#interview_quote_3')[0], $('#interview_quote_3'));
    var interview_quote_4 = new IsInView($('#interview_quote_4')[0], $('#interview_quote_4'));
    var second_interview_quote_1 = new IsInView($('#second_interview_quote_1')[0], $('#second_interview_quote_1'));
    var second_interview_quote_2 = new IsInView($('#second_interview_quote_2')[0], $('#second_interview_quote_2'));
    var second_interview_quote_3 = new IsInView($('#second_interview_quote_3')[0], $('#second_interview_quote_3'));
    var second_interview_quote_4 = new IsInView($('#second_interview_quote_4')[0], $('#second_interview_quote_4'));
    
    var animationCongi = {opacity: 1, top: top};
    
    $(this).on('scroll', function() {
        if(interview_quote_1.go()) {
          interview_quote_1.getView().animate(animationCongi, fadeTiming);
        }
        
        if(interview_quote_2.go()) {
          interview_quote_2.getView().animate(animationCongi, fadeTiming);
        }
        
        if(interview_quote_3.go()) {
          interview_quote_3.getView().animate(animationCongi, fadeTiming);
        }
        
        if(interview_quote_4.go()) {
          interview_quote_4.getView().animate(animationCongi, fadeTiming);
        }
        
        if(second_interview_quote_1.go()) {
          second_interview_quote_1.getView().animate(animationCongi, fadeTiming);
        }
        
        if(second_interview_quote_2.go()) {
          second_interview_quote_2.getView().animate(animationCongi, fadeTiming);
        }
        
        if(second_interview_quote_3.go()) {
          second_interview_quote_3.getView().animate(animationCongi, fadeTiming);
        }
    });
    
    topScroll($('#text_where'), $("#contact_container"), $('#top_bar').height());
    topScroll($('#text_team'), $("#team_container"), $('#top_bar').height());
    topScroll($('#text_contact'), $("#contact_container"), $('#top_bar').height());
    
});



