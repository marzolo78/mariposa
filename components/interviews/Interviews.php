<?php

class Interviews
{
    public $language;
    public $content;
    const IMAGE_PATH = "/static/images/interviews/";
    const BUTTONS_IMAGE_PATH = "/static/images/buttons/";
    const SHARED_IMAGE_PATH = "/static/images/shared/";
    const INTERVIEWS_JS = "/components/interviews/interviews.js";


    public function __construct($language) {
        $this->language = $language;
    }
    
    public function show()
    {
        $this->content = $this->getContent($this->language);
        include_once 'components/interviews/interviewsHtml.php';
    }
    
    public function includeJS()
    {
        return '<script src="' . self::INTERVIEWS_JS . '"></script>';
    }
    
    public function alternateView($num, $item)
    {
        $html = null;
        if($num % 2 != 0)
        {
            $html = '<div  class="interview_container interview_container_1">
                        <div class="interview_content_container">
                            <div id="interview_img_container_' . $num  . '" class="interview_img_container team_boxes">
                                <img id="interview_back_img_' . $num . '" class="center_horizontal" src="' . self::SHARED_IMAGE_PATH .  $item->imgBack . '" alt="interview_sabine_front"/>
                                <img id="interview_front_img_' . $num . '" class="center_horizontal" src="' . self::SHARED_IMAGE_PATH .  $item->imgFront . '" alt="interview_sabine_Back"/>
                                <img class="navi_buttons center_horizontal" src="' . self::BUTTONS_IMAGE_PATH .  $item->imgButton . '" alt="sabine_button"/>
                            </div>
                        </div>';
            foreach ($item->text->quotes as $key => $quote)
            {
                
                $html .=  '<div class="interview_content_container"><p id="interview_quote_' . ($key+1) . '" class="interview_quotes center_horizontal">»' .$quote . '«</p></div>';                    
            }
            $html .= '</div>';
            $html .= '<div class="interview_container interview_container_1">';
            foreach ($item->text->interview as $interview)
            {
               $html .=  $interview;
            }          
            $html .= '</div>';
            $html .= '<div class="interview_container interview_container_1">';
            foreach($item->galery as $img) {
                $html .= '<div class="interview_galery_container">';
                if($img != null):
                $html .= '<img src="' . self::IMAGE_PATH . $img . '" alt="' . $img . '"/>';
                endif;
                $html .= '</div>';
            }
            $html .= '</div>';
        }else
        {
            $html = '<div class="interview_container interview_container_2">';
                foreach ($item->text->interview as $interview)
                {
                   $html .=  $interview;
                }       
            $html .= '</div>';
            
            $html .= '<div class="interview_container interview_container_2">
                        <div class="interview_content_container">
                            <div id="interview_img_container_' . $num  . '" class="interview_img_container team_boxes">
                                <img id="interview_back_img_' . $num .   '" class="center_horizontal" src="' . self::SHARED_IMAGE_PATH .  $item->imgBack . '" alt="interview_rosi_front"/>
                                <img id="interview_front_img_' . $num .  '" class="center_horizontal" src="' . self::IMAGE_PATH .  $item->imgFront . '" alt="interview_rosi_back"/>
                                <img class="navi_buttons center_horizontal" src="' . self::BUTTONS_IMAGE_PATH .  $item->imgButton . '" alt="rosi_button"/>
                            </div>
                            </div>';
                        foreach ($item->text->quotes as $key => $quote)
                        {
                           $html .=  '<div class="interview_content_container"><p id="second_interview_quote_' . ($key+1) . '" class="interview_quotes center_horizontal">»' .$quote . '«</p></div>';
                        }
                        $html .= '<div class="interview_content_container">'
                                    . '<img id="interviews_butterf_green_3" class="absolute" src="' . self::SHARED_IMAGE_PATH . '/butterf_green.png" alt="interviews_butterf_green_3"/>'
                                    . '<img id="interviews_butterf_yellow_3" class="absolute" src="' . self::SHARED_IMAGE_PATH . '/butterf_yellow.png" alt="interviews_butterf_yellow_3"/>'
                                    . '<img id="interview_img_container" src="' . self::IMAGE_PATH . 'monster_05.png" alt="monster_05.png"/>'
                                . '</div>';
            $html .= '</div>';
            $html .= '<div class="interview_container interview_container_2">';
            foreach($item->galery as $key => $img) {
                $html .= '<div class="interview_galery_container interview_galery_height">';
                if($img !== null)
                {
                if($key === 0)
                {
                    $html .= '<img id="interviews_butterf_green_2" class="absolute" src="' . self::SHARED_IMAGE_PATH . '/butterf_green.png" alt="interviews_butterf_green_2"/>';
                    $html .= '<img id="interviews_butterf_yellow_2" class="absolute" src="' . self::SHARED_IMAGE_PATH . '/butterf_yellow.png" alt="interviews_butterf_yellow_2"/>';
                }
                $html .= '<img src="' . self::IMAGE_PATH . $img . '" alt="' . $img . '"/>';
                }
                $html .= '</div>';
            }
            $html .= '</div>';
        }
        
        return $html;
    }
    
    private function getContent($language) {
        $object = null;
        $json = json_decode(file_get_contents('components/interviews/content.json'));

        switch($language) {
            case Language::GERMAN:
                $object = $json->german;
                break;
            case Language::SPANISH:
                $object = $json->spanish;
                break;
        }
        
        return $object;
    }
}

