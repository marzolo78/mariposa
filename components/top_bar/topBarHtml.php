<div id="top_bar" class="bar top_bar_height">
    <div id="top_bar_top_content_container" class="top_bar_top_content_height main_width">
        <img id="top_bar_text" class="center_vertical" src="<?php echo self::IMGS_PATH ?>top_bar_text.png" alt="Kindergarten Mariposa e.V. - Spanisch-deutsche Elterninitiative"/>
        <div id="top_bar_navi" class="top_bar_top_content_height">
            <a href="https://www.facebook.com/Kindergarten-Mariposa-449623738568386/" target="_blank"><img class="language center_vertical" src="<?php echo self::IMGS_PATH . $this->content->topBar->visitUs ?>" alt="visit_us"/></a>
            <?php if($this->from !== "Error404" && $this->from !== "DataProtection"): ?>
            <a href="/<?php echo $this->setPageName ?>"><img id="language" class="language center_vertical navi_margin_left" src="<?php echo self::IMGS_PATH . $this->content->topBar->german ?>" alt="<?php echo $this->language; ?>"/></a>
            <img id="info" class="language center_vertical navi_margin_left" src="<?php echo self::IMGS_PATH . $this->content->topBar->info ?>" alt="info"/>
			<img id="contact" class="language center_vertical navi_margin_left" src="<?php echo self::IMGS_PATH . $this->content->topBar->contact ?>" alt="navi_kontakt"/>    
			<?php endif; ?>
        </div>
    </div>
    <div id="top_bar_sub_menu_container">
        <div id="top_bar_logo_container">
            <img class="center_vertical" src="<?php echo self::IMGS_PATH ?>mariposa_logo.jpg" alt="mariposa_logo"/>
        </div>
        <?php if($this->setPageName === PageName::MAIN_SPANISH || empty($this->setPageName) && ($this->from !== "Error404" && $this->from !== "DataProtection")): ?>
        <div id="navi_container">
            <img id="navi_how_we" class="center_vertical" src="<?php echo self::BUTTONS_IMGS_PATH . $this->content->subBar->whoAre?>" alt="wer_sind_wir"/>
            <img id="navi_what_offer" class="navi_img_margin_left center_vertical" src="<?php echo self::BUTTONS_IMGS_PATH . $this->content->subBar->whatOffer ?>" alt="navi_was_bitten_wir_green"/>
            <img id="navi_the_team" class="navi_img_margin_left center_vertical" src="<?php echo self::BUTTONS_IMGS_PATH . $this->content->subBar->theTeam ?>" alt="navi_das_team_green"/>
            <img id="navi_the_haus" class="navi_img_margin_left center_vertical" src="<?php echo self::BUTTONS_IMGS_PATH . $this->content->subBar->theHaus ?>" alt="navi_das_haus_green"/>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php if($this->from !== "Error404"): ?>
<div id="virtual_top_bar" class="top_bar_height"></div>
<?php endif; 

