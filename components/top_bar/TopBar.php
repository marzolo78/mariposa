<?php

class TopBar
{
    public $language;
    public $from;
    public $content;
    public $setPageName;
    const IMGS_PATH = "/static/images/top_bar/";
    const BUTTONS_IMGS_PATH = "/static/images/buttons/";
    const TOP_BAR_JS = "/components/top_bar/top_bar.js";


    public function __construct($language, $from = null) {
        $this->language = $language;
        $this->from = $from;
    }
    
    public function show()
    {
        $this->content = $this->getContent($this->language);
        include_once 'components/top_bar/topBarHtml.php';
    }
    
    public function includeJS()
    {
        return '<script src="' . self::TOP_BAR_JS . '"></script>';
    }
    
    private function getContent($language)
    {
        $object = null;
        $json = json_decode(file_get_contents('components/top_bar/content.json'));

        switch($language) {
            case Language::GERMAN:
                $object = $json->german;
                break;
            case Language::SPANISH:
                $object = $json->spanish;
                break;
        }
        
        return $object;
    }
    
    public function setPageName($pageName)
    {
        $this->setPageName = $pageName;
    }
}

