$(document).ready(function() {
    var fileNames = null;
    var language = getLanguage();
    
    if(language === 1) {
        fileNames = ["visit_us", "navi_espanol", "info", "kontakt_impressum"];
    }else if(language === 2) {
        fileNames = ["visit_us", "navi_deutsch", "info", "kontakt_impressum_es"];
    }
    
    var topBarNavi = new ChangeButtonColorGroup();
    topBarNavi.setMainContainer($('#top_bar_navi img'));
    topBarNavi.setImgPath('/static/images/top_bar/');
    topBarNavi.setFileNames(fileNames);
    topBarNavi.setFileFormat('png');
    topBarNavi.setColorOrder('green', 'red');
    topBarNavi.go();
    
    if(language === 1) {
        fileNames = ["wer_sind_wir", "was_bitten_wir", "das_team", "das_haus"];
    }else if(language === 2) {
        fileNames = ["quienes_somos", "que_ofrecemos", "el_equipo", "la_casa"];
    }
    
    var changeButtonColorGroup = new ChangeButtonColorGroup();
    changeButtonColorGroup.setMainContainer($('#navi_container img'));
    changeButtonColorGroup.setImgPath('/static/images/buttons/');
    changeButtonColorGroup.setFileNames(fileNames);
    changeButtonColorGroup.setFileFormat('svg');
    changeButtonColorGroup.setColorOrder('red', 'green');
    changeButtonColorGroup.go();
    
    var topBarHeight = $('#top_bar').height();
    topScroll($('#top_bar_text'), $("#main_container"), topBarHeight);
    topScroll($('#top_bar_logo_container'), $("#main_container"), topBarHeight);
	topScroll($('#info'), $("#info_container"), topBarHeight);
    topScroll($('#contact'), $("#contact_container"), topBarHeight);

    
    var topBarScrollTo = new ScrollTo();
    topBarScrollTo.setMainContainer($('#navi_container img'));
    topBarScrollTo.go();
    
    $('.language').on('click', function() {
        var name = $('.language').attr('name');
        if(name === "inicio" || name === "/") {
            var scrollPosition = $(document).scrollTop();
            var cookies = new Cookies();
            cookies.setCookies("scrollPosition", scrollPosition, 10);
        }
    });
});



