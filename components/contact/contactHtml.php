<div id="contact_container" class="components_margin_top">
    <div id="contact_info">
        <img src="<?php echo self::BUTTONS_IMGS_PATH . $this->content->buttonFile ?>" alt="mariposa_contact_title"/>
        <p class="intro contact_margin_top"><?php echo $this->content->address ?></p>
        <p class="answer text_size_medium contact_margin_top"><?php echo $this->content->text ?></p>
        <br/>
        <a href="/datenschutz" class="link question">Datenschutzerklärung</a>
    </div>
    <div id="contact_map_container">
    <img src="<?php echo self::IMGS_PATH . $this->content->mapFile ?>" alt="mariposa_contact_map"/>
    </div>
</div>

