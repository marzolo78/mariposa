<?php

class Contact
{
    public $language;
    public $content;
    const IMGS_PATH = "/static/images/contact/";
    const BUTTONS_IMGS_PATH = "/static/images/buttons/";
    const JS_PATH = "/components/contact/contact.js";


    public function __construct($language) {
        $this->language = $language;
    }
    
    public function show()
    {
        $this->content = $this->getContent($this->language);
        include_once 'components/contact/contactHtml.php';
    }
    
    public function includeJS()
    {
        return '<script src="' . self::JS_PATH . '"></script>';
    }
    
    private function getContent($language) {
        $object = null;
        $json = json_decode(file_get_contents('components/contact/content.json'));

        switch($language) {
            case Language::GERMAN:
                $object = $json->german;
                break;
            case Language::SPANISH:
                $object = $json->spanish;
                break;
        }
        
        return $object;
    }
}

